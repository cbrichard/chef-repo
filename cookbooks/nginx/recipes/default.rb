#
# Cookbook Name:: nginx
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Call apt cookbook to update repositories before installing nginx
include_recipe "apt"

# Create package source to make sure the nginx package is installed
package 'nginx' do
   action :install
end

# Create resource to start the nginx server once installed and on boot
service 'nginx' do
    action [ :enable, :start ]
end

# Resource to serve index.html to server once started
cookbook_file "/usr/share/nginx/www/index.html" do
    source "index.html"
    mode "0644"
end

